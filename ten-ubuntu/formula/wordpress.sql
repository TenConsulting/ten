/*! Create Database */
CREATE DATABASE AmericaThamizhan;
CREATE DATABASE CodeFastOnline;
CREATE DATABASE ConsultVignesh;
CREATE DATABASE DesisInTheUS;
CREATE DATABASE GreenCardGanesha;
CREATE DATABASE IdeasToBusiness;
CREATE DATABASE KaryaTeam;
CREATE DATABASE ParinamaAcademy;
CREATE DATABASE SmartopiaUS;
CREATE DATABASE SpicyIdli;
CREATE DATABASE Sportbase;
CREATE DATABASE TenConsulting;
CREATE DATABASE TingTingBaba;

/*! Create User */
CREATE USER
'marutagana'@'%'
IDENTIFIED BY 'password';

/*! Create User */
SET PASSWORD
FOR 'marutagana'@'%' = PASSWORD
('ExperimentPalaceVoyagePublic');

/*! Grant Privileges */
GRANT ALL PRIVILEGES
ON AmericaThamizhan.*
TO 'marutagana'@'%';
GRANT ALL PRIVILEGES
ON CodeFastOnline.*
TO 'marutagana'@'%';
GRANT ALL PRIVILEGES
ON ConsultVignesh.*
TO 'marutagana'@'%';
GRANT ALL PRIVILEGES
ON DesisInTheUS.*
TO 'marutagana'@'%';
GRANT ALL PRIVILEGES
ON GreenCardGanesha.*
TO 'marutagana'@'%';
GRANT ALL PRIVILEGES
ON IdeasToBusiness.*
TO 'marutagana'@'%';
GRANT ALL PRIVILEGES
ON KaryaTeam.*
TO 'marutagana'@'%';
GRANT ALL PRIVILEGES
ON ParinamaAcademy.*
TO 'marutagana'@'%';
GRANT ALL PRIVILEGES
ON ParinamaLLC.*
TO 'marutagana'@'%';
GRANT ALL PRIVILEGES
ON SmartopiaUS.*
TO 'marutagana'@'%';
GRANT ALL PRIVILEGES
ON SpicyIdli.*
TO 'marutagana'@'%';
GRANT ALL PRIVILEGES
ON Sportbase.*
TO 'marutagana'@'%';
GRANT ALL PRIVILEGES
ON TenConsulting.*
TO 'marutagana'@'%';
GRANT ALL PRIVILEGES
ON TingTingBaba.*
TO 'marutagana'@'%';


/*! Flush Privileges */
FLUSH PRIVILEGES;
